use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};

/// Input Json structure for the Lambda function.
#[derive(Deserialize)]
struct Request {
    a: i32,
    b: i32,
}

/// Output Json structure for the Lambda function.
#[derive(Serialize)]
struct Response {
    req_id: String,
    answer: i32,
}

/// Lambda Function 
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract integers from the request
    let a = event.payload.a;
    let b = event.payload.b;

    // Raise `a` to the power of `b` and store the result in `answer` variable.
    let answer = a.pow(b.try_into().unwrap());

    // Prepare the response
    let resp = Response {
        req_id: event.context.request_id.clone(),
        answer,
    };

    // Return `Response`
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    run(service_fn(function_handler)).await
}

